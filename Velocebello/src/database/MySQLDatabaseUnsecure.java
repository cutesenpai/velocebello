package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import data.Kunde;
import data.KundenListe;

public class MySQLDatabaseUnsecure {

	private Connection con;
	
	public void verbindungAufbauen() {
		try {
			// Parameter für Verbindungsaufbau definieren
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/velocebello?";
			String user = "root";
			String password = "";
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			con = DriverManager.getConnection(url, user, password);
		} catch (Exception ex) {
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
	}
	
	/**
	 * ein neuer Kunde soll mit einem INSERT Befehl eingetragen werden
	 * @param k - der Kunde
	 * @return true wenn die Aktion erfolgreich war
	 */
	public boolean neuerKunde(Kunde k) {
		try {
			//Verbindung aufbauen
			this.verbindungAufbauen();
			// Zeichenkette für SQL-Befehl erstellen
			String sql = "INSERT INTO t_kunden VALUES (" + k.getKundennummer() +",'" + k.getNachname() + "','" + k.getVorname() + "','" + k.getStrasse()		//Ist Fehler anfaellig, da z.B. der Telefonnummer wie folgt
						 + "','" + k.getOrt() + "','" + k.getPlz() + "','" + k.getTelefonnummer() + "');";													//aussehen kann: 5516323'); SELECT * FROM TKUNDEN WHERE (vorname='Hans
			// SQL-Anweisungen ausführen
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			// Verbindung schließen
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
			return false;
		}
		return true;
	}

	/**
	 * es soll die höchste Kundennummer der Datenbank aus der Kundentabelle
	 * zurückgegeben werden
	 * @return die höchste Kundennummer
	 */
	public int maxKundennummer() {
		int maxKundennummer = 0;
		try {
			//Verbindung aufbauen
			this.verbindungAufbauen();
			// Zeichenkette für SQL-Befehl erstellen
			String sql = "SELECT MAX(P_Kunden_Nr) FROM t_kunden";
			// Statement erstellen
			Statement stmt = con.createStatement();
			// SQL-Anweisungen ausführen
			ResultSet rs = stmt.executeQuery(sql);
			// ResultSet auswerten
			if (rs.next())
				maxKundennummer = rs.getInt(1);

			// Verbindung schließen
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return maxKundennummer;
	}

	/**
	 * Es soll die komplette Kundenliste zurückgegeben werden, nach Nachnamen, Vornamen sortiert
	 * @return sortierte Kundenliste
	 */
	public KundenListe getKundenliste() {
		KundenListe kundenliste = new KundenListe();
		try {
			//Verbindung aufbauen
			this.verbindungAufbauen();
			// Zeichenkette für SQL-Befehl erstellen
			String sql = "SELECT * FROM t_kunden ORDER BY P_Kunden_Nr";
			// Statement erstellen
			Statement stmt = con.createStatement();
			// SQL-Anweisungen ausführen
			ResultSet rs = stmt.executeQuery(sql);
			// ResultSet auswerten
			while (rs.next()) {
				Kunde k = new Kunde(rs.getInt("P_Kunden_Nr"), rs.getString("Nachname"), rs.getString("Vorname"),
						rs.getString("Strasse"), rs.getString("Ort"), rs.getString("PLZ"),
						rs.getString("Telefonnummer"));
				kundenliste.addKunde(k);
			}
			// Verbindung schließen
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return kundenliste;
	}

	// ------------------------ FINGER WEG VON DIESEM TEIL DES QUELLCODES ----------------------------------------------------------

	/**
	 * Erstellt ggf. eine Datenbank mit Beispieldaten 
	 * Voraussetzung: MySQL-Datenbank gestartet 
	 * FINGER WEG VON DIESEM TEIL DES QUELLCODES
	 */
	public void setup() {
		try {
			// Parameter für Verbindungsaufbau definieren
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/?";
			String user = "root";
			String password = "";
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausführen
			Statement stmt = con.createStatement();
			stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS velocebello;");
			// Verbindung schließen
			con.close();
			url = "jdbc:mysql://localhost/velocebello?";
			// Verbindung aufbauen
			con = DriverManager.getConnection(url, user, password);
			stmt = con.createStatement();
			// Kundentabelle einfügen
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS t_kunden(" + "P_Kunden_Nr INT PRIMARY KEY,"
					+ "Nachname VARCHAR(50) NOT NULL," + "Vorname VARCHAR(50) NOT NULL," + "Strasse VARCHAR(50),"
					+ "Ort VARCHAR(50)," + "PLZ INT," + "Telefonnummer VARCHAR(50));");
			// prüfen, ob DB bereits existiert und gefüllt ist
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM t_kunden;");
			boolean insert = true;
			if (rs.next())
				insert = rs.getInt(1) == 0;
			// ggf. Startdatensätze einfügen
			if (insert) {
				stmt.executeUpdate(
						"INSERT INTO TKunden VALUES ('1','Da Giorni', 'Filipe', 'Hofweg 6', 'Berlin', '12045', '030/215781'),"
								+ "('2', 'Conradt', 'Justin', 'Solweg 5', 'Berlin', '12055', '0176/1575487'),"
								+ "('3', 'Meyer', 'Emma', 'Finkweg 3', 'Zossen', '22334', '030/32856465'),"
								+ "('4', 'MC Gordon', 'Bary', 'Weserstr. 17', 'Berlin', '12059', null),"
								+ "('5', 'Malska', 'Katarina', 'Sonnenallee 137', 'Berlin', '12053', '0168/21545454'),"
								+ "('6', 'Blagus', 'Yvonne', 'Richardplatz 12a', 'Berlin', '12049', '030/35205487')");
			}
			con.close();

		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
	}

}
