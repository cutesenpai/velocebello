package view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import data.Kunde;
import data.KundenListe;
import logic.VelocebelloController;

public class VelocebelloGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private VelocebelloController vbc;
	private JTextField tfdKundennummer;
	private JTextField tfdVorname;
	private JTextField tfdNachname;
	private JTextField tfdStrasse;
	private JTextField tfdPlz;
	private JTextField tfdOrt;
	private JTextField tfdTelefonnummer;
	private JLabel lblFehlermeldung;
	private JPanel pnlCenter;
	private JTable tblKundenliste;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VelocebelloGUI frame = new VelocebelloGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VelocebelloGUI() {
		// Logikschicht
		this.vbc = new VelocebelloController();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 450);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(238, 232, 170));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JLabel lblVelocebelloInhE = new JLabel();
		lblVelocebelloInhE.setHorizontalAlignment(SwingConstants.CENTER);
		lblVelocebelloInhE.setFont(new Font("Comic Sans MS", Font.PLAIN, 30));
		lblVelocebelloInhE.setText("Velocebello - Inh. E. Z. Abel");
		contentPane.add(lblVelocebelloInhE, BorderLayout.NORTH);

		JPanel pnlSidebar = new JPanel();
		pnlSidebar.setBackground(new Color(238, 232, 170));
		contentPane.add(pnlSidebar, BorderLayout.WEST);
		pnlSidebar.setLayout(new GridLayout(0, 1, 0, 0));

		JButton btnKundenliste = new JButton("Kundenliste");
		btnKundenliste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnKundenliste_Clicked();
			}
		});
		pnlSidebar.add(btnKundenliste);

		JButton btnNeuerKunde = new JButton("Neuer Kunde");
		btnNeuerKunde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNeuerKunde_Clicked();
			}
		});
		pnlSidebar.add(btnNeuerKunde);

		JButton btnNeueReparatur = new JButton("Neue Reparatur");
		btnNeueReparatur.setEnabled(false);
		pnlSidebar.add(btnNeueReparatur);

		JButton btnFahrradliste = new JButton("Fahrradliste");
		btnFahrradliste.setEnabled(false);
		pnlSidebar.add(btnFahrradliste);

		pnlCenter = new JPanel();
		pnlCenter.setBackground(new Color(238, 232, 170));
		contentPane.add(pnlCenter, BorderLayout.CENTER);
		pnlCenter.setLayout(new CardLayout(0, 0));

		tblKundenliste = new JTable(vbc.getKundenliste().getKunden(), KundenListe.SPALTENNAMEN);
		tblKundenliste.setFillsViewportHeight(true);

		JScrollPane spnCardKundenliste = new JScrollPane(tblKundenliste);
		spnCardKundenliste.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pnlCenter.add(spnCardKundenliste, "Kundenliste");

		JPanel pnlCardNeuerKunde = new JPanel();
		pnlCenter.add(pnlCardNeuerKunde, "Neuer Kunde");
		pnlCardNeuerKunde.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlCardNeuerKunde.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel lblNeuerKunde = new JLabel("Neuer Kunde");
		lblNeuerKunde.setHorizontalAlignment(SwingConstants.CENTER);
		lblNeuerKunde.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		pnlCardNeuerKunde.add(lblNeuerKunde);

		JLabel lblKundennummer = new JLabel("Kundennummer");
		pnlCardNeuerKunde.add(lblKundennummer);

		tfdKundennummer = new JTextField();
		tfdKundennummer.setColumns(10);
		pnlCardNeuerKunde.add(tfdKundennummer);

		JLabel lblVorname = new JLabel("Nachname");
		pnlCardNeuerKunde.add(lblVorname);

		tfdVorname = new JTextField();
		tfdVorname.setColumns(10);
		pnlCardNeuerKunde.add(tfdVorname);

		JLabel lblNachname = new JLabel("Vorname");
		pnlCardNeuerKunde.add(lblNachname);

		tfdNachname = new JTextField();
		tfdNachname.setColumns(10);
		pnlCardNeuerKunde.add(tfdNachname);

		JLabel lblStrasse = new JLabel("Strasse");
		pnlCardNeuerKunde.add(lblStrasse);

		tfdStrasse = new JTextField();
		tfdStrasse.setColumns(10);
		pnlCardNeuerKunde.add(tfdStrasse);

		JLabel lblPlz = new JLabel("Ort");
		pnlCardNeuerKunde.add(lblPlz);

		tfdPlz = new JTextField();
		tfdPlz.setColumns(10);
		pnlCardNeuerKunde.add(tfdPlz);

		JLabel lblOrt = new JLabel("PLZ");
		pnlCardNeuerKunde.add(lblOrt);

		tfdOrt = new JTextField();
		tfdOrt.setColumns(10);
		pnlCardNeuerKunde.add(tfdOrt);

		JLabel lblTelefonnummer = new JLabel("Telefonnummer");
		pnlCardNeuerKunde.add(lblTelefonnummer);

		tfdTelefonnummer = new JTextField();
		tfdTelefonnummer.setColumns(10);
		pnlCardNeuerKunde.add(tfdTelefonnummer);

		lblFehlermeldung = new JLabel("");
		lblFehlermeldung.setHorizontalAlignment(SwingConstants.CENTER);
		lblFehlermeldung.setForeground(Color.RED);
		pnlCardNeuerKunde.add(lblFehlermeldung);

		JButton button = new JButton("Eintragen");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEintragen_Clicked();
			}
		});
		pnlCardNeuerKunde.add(button);
	}

	/**
	 * Anzeigen des Formulars Neuer Kunde
	 */
	protected void btnNeuerKunde_Clicked() {
		// Formular anzeigen
		((CardLayout) pnlCenter.getLayout()).show(pnlCenter, "Neuer Kunde");
		// Kundennummer eintragen
		tfdKundennummer.setText(vbc.ermittleNaechsteKundennummer() + "");
	}

	/**
	 * Anzeigen der Kundenliste
	 */
	public void btnKundenliste_Clicked() {
		// Tabelleninhalt aktualisieren
		tblKundenliste.setModel(new DefaultTableModel(vbc.getKundenliste().getKunden(), KundenListe.SPALTENNAMEN));
		// Tabelle anzeigen
		((CardLayout) pnlCenter.getLayout()).show(pnlCenter, "Kundenliste");
	}

	/**
	 * Eintragen eines Kunden aus dem Formular Neuer Kunde
	 */
	public void btnEintragen_Clicked() {
		// Fehlermeldung zurücksetzen
		lblFehlermeldung.setText("");
		boolean ergebnis = false;
		// Castfehler abfangen - nicht ganzzahlige Kundennummer
		try {
			// Kundenobjekt anlegen
			Kunde k = new Kunde(Integer.parseInt(tfdKundennummer.getText()), tfdNachname.getText(),
					tfdVorname.getText(), tfdStrasse.getText(), tfdOrt.getText(), tfdPlz.getText(),
					tfdTelefonnummer.getText());
			// Logik beauftragen neuen Kunden anzulegen
			ergebnis = this.vbc.neuerKunde(k);

			// Rückmeldung an Benutzer
			if (ergebnis) {
				lblFehlermeldung.setText("Kunde erfolgreich eingetragen");
			} else {
				lblFehlermeldung.setText("Datenbankfehler - DB gestartet?");
			}

		} catch (NumberFormatException e) {
			lblFehlermeldung.setText("Falsches Kundennummerformat");
		}

	}
}
